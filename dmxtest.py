#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Stinca
# v => 0.1
# last modification: 20180227

from DmxPy import DmxPy
import time
dmx = DmxPy('/dev/ttyUSB0')
while True:
	dmx.setChannel(1, 100)
	dmx.setChannel(2, 50)
	dmx.render()
	time.sleep(2)
	dmx.setChannel(3, 100)
	time.sleep(2)
	dmx.blackout()
	dmx.render()
	time.sleep(2)
